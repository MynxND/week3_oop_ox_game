/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Mynx
 */
public class TestBoard {

    public TestBoard() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 0);
        board.setRowCol(0, 1);
        board.setRowCol(0, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());
    }

    @Test
    public void testRow2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(1, 0);
        board.setRowCol(1, 1);
        board.setRowCol(1, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());
    }

    @Test
    public void testRow3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(2, 0);
        board.setRowCol(2, 1);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());
    }

    @Test
    public void testCol1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 0);
        board.setRowCol(1, 0);
        board.setRowCol(2, 0);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());

    }

    @Test
    public void testCol2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 1);
        board.setRowCol(1, 1);
        board.setRowCol(2, 1);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());

    }

    @Test
    public void testCol3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 2);
        board.setRowCol(1, 2);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());

    }

    @Test
    public void testDiagonal1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 2);
        board.setRowCol(1, 1);
        board.setRowCol(2, 0);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());

    }

    @Test
    public void testDiagonal2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.setRowCol(0, 0);
        board.setRowCol(1, 1);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(x, board.getWinner());

    }

    @Test
    public void testRow1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 0);
        board.setRowCol(0, 1);
        board.setRowCol(0, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());
    }

    @Test
    public void testRow2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(1, 0);
        board.setRowCol(1, 1);
        board.setRowCol(1, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());
    }

    @Test
    public void testRow3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(2, 0);
        board.setRowCol(2, 1);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());
    }

    @Test
    public void testCol1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 0);
        board.setRowCol(1, 0);
        board.setRowCol(2, 0);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());

    }

    @Test
    public void testCol2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 1);
        board.setRowCol(1, 1);
        board.setRowCol(2, 1);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());

    }

    @Test
    public void testCol3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 2);
        board.setRowCol(1, 2);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());

    }

    @Test
    public void testDiagonal1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 2);
        board.setRowCol(1, 1);
        board.setRowCol(2, 0);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());

    }

    @Test
    public void testDiagonal2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table board = new Table(x, o);
        board.switchPlayer();
        board.setRowCol(0, 0);
        board.setRowCol(1, 1);
        board.setRowCol(2, 2);
        board.checkWin();
        assertEquals(true, board.getIsfinish());
        assertEquals(o, board.getWinner());

    }
 
}
