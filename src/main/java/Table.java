/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mynx
 */
import java.util.*;

public class Table {

    int count = 0;
    int col, row;
    boolean isFinish;
    Player playerX;
    Player playerO;
    Player currentPlayer;
    Player winner;
    int turn;
    char[][] board = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    Scanner sc = new Scanner(System.in);

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
        isFinish = false;
        turn = 1;

    }

    public void showTable() {

        System.out.println();

        System.out.println(" 1 2 3");
        for (int row = 0; row < 3; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + " ");

            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int Row, int Col) {
        if (board[Row][Col] == '-') {
            board[Row][Col] = currentPlayer.getName();
            return true;
        } else {
            return false;
        }
    }

    public void setIsFinish() {
        isFinish = true;
    }

    public boolean getIsfinish() {
        return isFinish;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }

    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2]
                    && board[row][0] != '-') {
                winner = currentPlayer;
                setIsFinish();
                break;
            }
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col]
                    && board[0][col] != '-') {
                winner = currentPlayer;
                setIsFinish();
                break;
            }
        }
    }

    public void checkDiagonal() {
        for (int i = 0; i < 3; i++) {
            if (board[i][i] != currentPlayer.getName()) {
                return;
            }

        }
        isFinish = true;
        winner = currentPlayer;

    }

    public void checkDiagonal2() {
        for (int i = 0; i < 3; i++) {
            if (board[i][3 - 1 - i] != currentPlayer.getName()) {
                return;
            }

        }
        isFinish = true;
        winner = currentPlayer;

    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        checkDiagonal2();
        if (count == 9) {
            isFinish = true;
        }
    }

    public void turn() {
        turn++;
    }

    public Player getWinner() {
        return winner;
    }

}
