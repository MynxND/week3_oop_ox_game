/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mynx
 */
import java.util.*;

public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table board;
    Scanner sc = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        board = new Table(playerX, playerO);

    }

    public void showWelcome() {
        System.out.println("Weelcome to OX game");
    }

    public void showTable() {
        board.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row col:");
            int row = sc.nextInt() - 1;
            int col = sc.nextInt() - 1;
            if (board.setRowCol(row, col)) {
                board.turn();
                return;
            }
            System.out.println("Error table at row and col is not Empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(board.getCurrentPlayer().getName() + " turn");
    }

    public void switchPlayer() {
        board.switchPlayer();
    }

    public void showResult() {
        if (board.getWinner() == null) {
            System.out.println("Draw !!!");
            System.out.println("Bye bye");
        } else {
            System.out.println("Player " + board.getWinner().getName() + " Win....");
            System.out.println("Bye bye");
        }

    }

    public void run() {
        int count = 1;
        showWelcome();
        showTable();
        do {
            showTurn();
            input();
            showTable();
            board.checkWin();
            switchPlayer();

        } while (!board.isFinish == true);
        showResult();

    }

}
